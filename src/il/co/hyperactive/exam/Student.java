package il.co.hyperactive.exam;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

public class Student implements Serializable{

	private String name;
	private String lastName;
	private String id;
	private String phoneNumber;
	private String adress;
	private Hashtable<Date, String> comments;
	private ArrayList<Group> groups;
	
	public Student(String name, String lastName, String id, String phoneNumber, String adress) {
		this.name = name;
		this.lastName = lastName;
		this.id = id;
		this.phoneNumber = phoneNumber;
		this.adress = adress;
		comments=new Hashtable<Date,String>();
		groups=new ArrayList<Group>();
	}
	
	public String getName() {
		return name;
	}
	
	public void addComment(String txt)
	{
		try 
		{
			Thread.sleep(500);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Date date=new Date();		
		comments.put(date, txt);
	}
	
	@Override
	public String toString() {
		StringBuffer result=new StringBuffer(100);
	
		result.append("Student [name=").append(name).append(", lastName=").append(lastName);
		result.append(", id=").append(id).append(", phoneNumber=").append(phoneNumber);
		result.append(", adress=").append(adress).append("]\n");
		
		result.append("List of comments that "+name+" received: \n");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		//option1
		ArrayList<Date> dates = Collections.list(comments.keys());
		//Collections.sort(dates);		
		
		for (Date date : dates) {
			result.append(dateFormat.format(date)).append(" ").append(comments.get(date));
			result.append("\n");
		}
		/* option2
		 Enumeration<Date> dates=comments.keys();
		 while(dates.hasMoreElements()){
			 result.append(dateFormat.format(dates.nextElement())+" "+comments.get(dates.nextElement()));
			 result.append("\n");
		 }
		 */
		result.append("the student is signed to these groups: \n");
		for (Group group : groups) {
			result.append(group.getName());
			result.append("\n");
		}
		return result.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(!(obj instanceof Student))
			return false;
		Student otherStudent=(Student)obj;
		return this.id.equals(otherStudent.id);
	}
	
	public void signUpToGroup(Group group){
		this.groups.add(group);
		group.addStudent(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

}
