package il.co.hyperactive.exam;

import java.io.Serializable;
import java.util.ArrayList;

public class Class implements Serializable{
	
	private ArrayList<Student> students;
	private ArrayList<Group> groups;
	
	public Class()
	{
		students=new ArrayList<Student>();
		groups=new ArrayList<Group>();
	}
	
	public boolean addStudent(Student student){
		if(!students.contains(student)){
			students.add(student);
			return true;
		}
		return false;
	}
	
	public boolean addGroup(Group group){
		if(!groups.contains(group)){
			groups.add(group);
			return true;
		}
		return false;
	}

	
	public ArrayList<Student> getStudents() {
		return students;
	}

	

	public ArrayList<Group> getGroups() {
		return groups;
	}

	

	
}
