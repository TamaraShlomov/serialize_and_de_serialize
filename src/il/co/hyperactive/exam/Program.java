package il.co.hyperactive.exam;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

public class Program {

	public static void main(String[] args) {
		
		Class bSClass=new Class();
		Student student1=new Student("tom1", "shlomov", "312984834", "0542151182", "aaa");
		Student student2=new Student("tom2", "shlomov", "312984835", "0542151182", "aaa");
		Student student3=new Student("tom3", "shlomov", "312984836", "0542151182", "aaa");
		bSClass.addStudent(student1);
		bSClass.addStudent(student2);
		bSClass.addStudent(student3);
		Group math=new Group("math", "matematika","1");
		Group java=new Group("java", "bla","2");
		Group cSharpe=new Group("c#", "bla bla","3");
		bSClass.addGroup(math);
		bSClass.addGroup(java);
		bSClass.addGroup(cSharpe);
		student1.signUpToGroup(math);
		student1.signUpToGroup(java);
		student1.signUpToGroup(cSharpe);
		student2.signUpToGroup(java);
		student3.signUpToGroup(cSharpe);
		student1.addComment("comment#1");
		student1.addComment("comment#2");
		student2.addComment("comment#1");
		student2.addComment("comment#2");
		student3.addComment("student3 comment");
		for (Student student : bSClass.getStudents())
			System.out.println(student);
		for (Group group : bSClass.getGroups())
			System.out.println(group);
		try 
		{
			FileOutputStream fos = new FileOutputStream("C:\\Users\\user\\Desktop\\Tomsjava_exam.txt");
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(bSClass);
			oos.close();
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		bSClass=null;
		student1=null;
		student2=null;
		student3=null;
		math=null;
		java=null;
		cSharpe=null;
		
		try
		{
			FileInputStream fis = new FileInputStream("C:\\Users\\user\\Desktop\\Tomsjava_exam.txt");
			ObjectInputStream ois=new ObjectInputStream(fis);
			bSClass=(Class)ois.readObject();
			ois.close();
			System.out.println("de-Serialize");
			for (Student student : bSClass.getStudents())
					System.out.println(student);
			for (Group group : bSClass.getGroups())
				System.out.println(group);
			
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
		
}
