package il.co.hyperactive.exam;

import java.io.Serializable;
import java.util.ArrayList;

public class Group implements Serializable{
	
	private String name;
	private String description;
	private String id;
	private ArrayList<Student> students;
	
	public String getName() {
		return name;
	}

	public Group(String name, String description,String id) {
		this.name = name;
		this.description = description;
		this.id=id;
		students=new ArrayList<Student>();
	}

	@Override
	public String toString() {
		StringBuffer result=new StringBuffer(100);
		result.append("Group [name=").append(name).append(", description=");
		result.append(description).append("]\n");	
		result.append("list of the students that member in these group:\n");
		for (Student student : students) {
			result.append(student.getName());
			result.append("\n");
		}
		return result.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(!(obj instanceof Group))
			return false;
		Group other=(Group)obj;
		return this.id.equals(other.id);
	}

	public void addStudent(Student student){
		students.add(student);
	}	
}
